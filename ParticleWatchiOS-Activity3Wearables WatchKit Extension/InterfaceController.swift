//
//  InterfaceController.swift
//  ParticleWatchiOS-Activity3Wearables WatchKit Extension
//
//  Created by AJAY BAJWA on 2019-11-03.
//  Copyright © 2019 ajay. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
